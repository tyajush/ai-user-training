**Neural Network**

A neural network consists of multiple perceptrons. Perceptrons are basically mathematical model for biological neuron similar to the one found in human brain. A DNN consists of multiple such neurons or perceptrons with multiple layers. The perceptron consists of input , applies activation function on it and then gives the output. Consider , y=w*x+b , here w is the weight , x is the input, b is the bias and y is final output given by the perceptron. 
'b' is the value called bias, it basically means that the value x*w must overcome value b in order to have output. 'w' is the weight and it has to do with the process called back propogation , lets understand what is back propogation.


**Gradient Descent**

Gradient descent is the algorithm that is used for implementing back propogation. Basically , it does two things , calculate gradients of loss function. Calculate gradients of loss funciton and then updating paramenters to reduce loss function by iteratively moving in direction of steepest descent. 
The iterative steps are very small because large step results in missing minimum value of cost function, this is called overfitting. These step sizes are known as **learning rate**. 
  **Adaptive gradient descent** is the modified version of simple gradient descent. In this it starts with the large steps and goes smaller as it realizes that slope is getting closer to 0 that  is it is getting less steeper.



**Back propogation**

In the input of the perceptron we multiply value w to x. w is changed by going back the neural network , the value is changed such that we get the expected value or the required output. The process of going back in network and changing the value of weights w is called as backpropogation. We basically keeps changing the value of weight w and keeps decreasing the value of cost function. 
**Cost function** is the measure of how far off we are from the correct output.
